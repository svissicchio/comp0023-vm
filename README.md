# COMP0023 VM

This repository contains the VM used as a basis for COMP0023 courseworks.
Instructions on how to install the VM with VirtualBox are reported at https://docs.google.com/document/d/1Bh7YN6VT-1oXbc3HPobiFnG0EvYpASGrQsxSojnEXX8

Note that to complete COMP0023 CWs, you are also allowed to import use other virtualization platforms/hypervisors, including for example VMWare ones.
